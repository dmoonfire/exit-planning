## [0.1.1](https://gitlab.com/dmoonfire/exit-planning/compare/v0.1.0...v0.1.1) (2022-04-20)


### Bug Fixes

* cleaning up some elements, adding getting started ([db6a980](https://gitlab.com/dmoonfire/exit-planning/commit/db6a9808082ca7f44039fad1229823908d8f9ea9))
