# Exit Planning

> They say death and taxes are the only things that are inevitable. The truth is, you can not pay your taxes. I've done it, and there's consequences, but it can be done. Death you're not going to get out of, and you kind of got to deal with it.
>
> --- Steve Earle

It isn't a question if you are going to die, it is a matter of what you leave behind. This repository talks about planning for that end.

## Starting Point

The best place to start is the [index](./src/index.md) page which has a links to everything else.

## Support

Since someone ask about it, if you like this and want to throw a few cents, consider [supporting the author](https://d.moonfire.us/support/).
