{
  description = "A commissioned novella by t&#39;Sade.";

  inputs = {
    nixstable.url = "nixpkgs/21.05";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, nixstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      	  stable = nixstable.legacyPackages.${system};
      in {
        devShell =
          pkgs.mkShell { buildInputs = [ pkgs.epubcheck pkgs.nixfmt pkgs.nodejs-16_x pkgs.pandoc pkgs.pdftk stable.python39Full stable.python39Packages.weasyprint ]; };
      });
}
