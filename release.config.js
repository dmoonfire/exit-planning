module.exports = {
    branches: ["main", "master"],

    extends: ["@commitlint/config-conventional"],
    message:
        "chore(release): v${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",

    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
            },
        ],
        "@semantic-release/release-notes-generator",
        "@semantic-release/npm",
        [
            "@semantic-release/exec",
            {
                prepareCmd: "npm run build",
            },
        ],
        "@semantic-release/changelog",
        "@semantic-release/git",
        //[
        //    "@semantic-release/gitlab",
        //    {
        //        assets: [
        //            { path: "*.epub", label: "EPUB2", type: "other" },
        //            { path: "*.pdf", label: "PDF", type: "other" },
        //        ],
        //    },
        //],
    ],
};
