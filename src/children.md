# Children

Get a [will](./legal.md).

Make sure everyone knows where the will is.

Seriously, this is important for a number reasons. One is that this is a [first hour](./time.md) knowledge because if someone has to go to the hospital or somewhere, you need to know the children are safe. This includes pets and other dependencies; we don't really make a distinction because it is a living being that you were taking care which means someone else needs to. Even your fish need to eat and knowing who is going to do that is important.

# First Days

Also, be mindful of the situation. If your littles are faced with the news that their caretaker is dead, leaving them along with their thoughts is not a good thing. You want to make sure the people watching over them are empathic and compassionate but also able to function. Sending them over to an aunt who will be sobbing non-stop for three days is not healthy... for anyone.

In those first few hours or even days, you want to make sure your children are taken care of someone who will help them handle the grief and prevent your littles from getting into a spiral of despair and fear. Keeping them safe is important.

# Later Days

Outside of those early days, you want to make sure you have plans for who will take care of them. This shouldn't be one person, it should be a series of trusted people and given in the order of preference.

> In case something goes wrong, send the kids to their grandmother. If she is not available, then try this aunt, and then this one. Their phone numbers and addresses are....

Wills are important when it comes to children because this is one of those things that if you do not plan for it, the government may do it for you. That means sending your kids to strangers or foster parents.

In your will, you'll also have to describe who handles the money for your children. This comes from life insurance, trusts, and beneficiaries, and the list. For this, you also want a list of people you trust to handle finances.

If possible, you do not want to the same person watching your children to manage the money for your children. It seems like a logical view, but money is a temptation for everyone. A lot of money, as insurance payments can be, are a bigger one. This is one reason why many churches have two people dealing with money, a separation between the money that comes in and the money that goes.

Sadly, this applies to caretakers also. The person taking care of you in a nursing home or handling your bills probably shouldn't be the person handling your money. We've seen some cases where a "caring" family member opened up lines of credit against the person who they were supposed to be taking care of, or using the incoming money to pay their own bills.
