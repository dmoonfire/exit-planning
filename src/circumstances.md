# Circumstances

None of us know how we are going to leave the world. We may go in our sleep or it could happen suddenly. We might have days to say good-bye or we may only have the shortest second before the end. We also learned with frequent hospitalizations with COVID, the chance to say final words might be hours or even days before it actually happens.

But, no matter how we leave the world, we leave others behind. Our survivors which may be our spouses, children, pet, or friends. There are others who have (or may want to) pick up the pieces to keep surviving themselves.

Exit planning is not about you, it is about the survivors. It is making sure we cover the bases to make their [first days or weeks](./time.md) are supported because those moments are the worst moments of their lives.

When we started coming up with exit planning, it came from experiences of getting a phone call about a parent dying in their sleep, or watching our parents prepare for their end. It is the what-if scenarios that rush by when someone is a few hours late coming home or that moment you pull your shaking your hands from the steering wheel of a near accident.

This also means we need to plan knowing that the survivors may be utterly devastated. They have just lost you hours or even minutes ago. They have to be able to make choices because life won't let them stop and feel grief. There are things they need to know while talking to the doctors or the police. There are other things that need to be handled the day after or the week after.

So we partition and categorize for them. We say "you need to pay attention to this and forget everything else until tomorrow." We give the priorities knowing both ours and their lives, how they handle stress, how they grieve.

Dying is terrifying for everyone. It is the unknown end for us but also this gaping hole in their lives. How do they pick up their lives? How do they make sure the bills are paid, the house is taken care of, what if they are next?

## Incapacitation

There are other reasons for needing to plan, and that is the case when you have a stroke, heart attack, or somehow become completely incapacitated and unable to function. Most of the same rules apply. A percentage of stroke victims never recover and it could be months or even years of incapacitation that require a [caretaker](./caretakers.md) and much of the same recovery.

As much as we wish it otherwise, an "exit plan" also covers these situations. There is a chance you might be able to give hints or suggestions or fixes, but plan as if you are unable to.
