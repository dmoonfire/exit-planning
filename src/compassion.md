# Compassion

Planning for the end is a compassion act. It is trying to ease our passing to the ones who survive. It is easy to just write down details, tasks, and items to do. You think of them as you do the plan or while you go about your life.

> "Oh, I love this new car. I better update where I'm putting the title."

There is another component to this, it is probably a grieving person reading your plan and you aren't there. We need to acknowledge that in addition to the pieces, but at the same time, keep them relatively separate and not distracting.

The first page in our [file](./documentation.md) is a just a little and nothing else.

> Take a deep breath.
>
> You have this.

It's going to be the first thing they see, the first thing they may always see when they go into the file. They need to know not to be scared or terrified. They need to know they can survive without you because they have to survive without you.

It probably wouldn't hurt to have other personal things in there. This is a way of comforting through the future.

> Dylan: I like writing poems and letters. So I use the pages to signify the [time](./time.md) categories with little notes for my partner.

Feel free to put a poem, a picture, or something that is important. Maybe a letter. It's that personal connection that is important. It is quite possible they have to put it aside, but those mementos might also be what keeps them going in the long nights that follow.

Obviously, you don't know who is actually going to be reading the file or they have company. Also, this file is used when you can't help you might still be around, so this is not the time to confess to affairs, murders, or crimes. Don't ruin it for them.

## Consider the Future

The movie, _P.S. I Love You_ (2007) is an inspiration for this, but write letters in the future. We can't be there for them, but we can write something that might help.

> Dylan: I have a few letters in letter that are sealed envelopes. They are marked with simple phrases like "When You Miss Me" or "When Things Get Too Much" and they are just little notes of encouragement. I know my partner might go through them all in a day or they may dole it out over years, but they are there.
