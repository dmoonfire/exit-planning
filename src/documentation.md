# Documentation

For us, the biggest driver for these plans is compassion. We know we leave people behind and, in most cases, they will not be prepared for us to be gone. Too easily, we can imagine they are sitting with a box in the middle of the kitchen floor, sobbing as they struggle to hold together their lives and deal with grief.

The plan is to help them through this. This means not assuming they know your passwords or know where to find the bank accounts. If they can't find it, they may lose something important or not realizing there is something they need.

This leads us to the [first tenant](./tenants.md):

> If it isn't written down, it doesn't exist.

Documentation is critical to plan. The more that can be laid out, the better. However, you need to also realize they don't have your skills. Leaving documentation that says "use my SSH key to log into my website and then go to ~/docs/start.md" isn't going to help someone who doesn't know what SSH is.

So a basic plan is just lists and directions. They can be dry and they can be plain, but they need to be written down. Somewhere. On paper or on a computer, but it needs to be somewhere they can get it.

## Start Simple

Start simple. Plain, just a few words. If you try to document all the things at once, it will quickly become overwhelming. So, to avoid that, just add a few details every time you [review](./review.md). Expand or clarify. Try to see it as if you don't know what you already know.

## Origin Documentation

Your documentation needs to start from a single, well-known point. This can be a manila envelope in a file cabinet or thrown in the back of the closet. The key part is that it is [secure](./security.md) and [trusted](./trust.md) people know where to find it.

If it is in your house, make sure they have keys or they know how to get in your house. You do not want the origin to be in a safe deposit box because that requires legal paperwork and probably a key to get it out.

This origin is where you start. It is going to be the first thing they open when they need to work through your plan. Now, it can contains pointers to other pieces of information like your [passwords](./passwords.md) or [legal paperwork](./legal.md), but generally you should view it as the start of all things in your plan.

The origin documentation shouldn't require a computer to access. It's okay if some of your information is technical, but you want the first part to be as easy to access as possible. You might even want a [backup](./security.md) in a different location. This means, paper is a good choice.

## Paper Instructions

This information will changes over time, so in our file, we use a blank sheet of 8x11" paper (or A5 like I prefer but can't use) for every topic. This allows us to easily rewrite a page that has changed without rewriting an entire page. Or just cross out the old information and add new stuff to the page.

From our experience, there are three useful pieces for the top of the page: title, [time category](./time.md), and date. The date is to let you know when you last updated the file. If its been a few years, you might need to look at it a bit more in detail. The title is just a hint of what the page contains. The category lets them focus only on what's important in that moment. Doing it this way also lets them page through the headers to find the section they need.

## Clarity

You also have to be honest about your handwriting here. If people regularly tell you that they can't tell your lettering from the random scribbles of a two year old, you should probably type everything up.

My cursive is horrible, but I have a decent block print. So my documentation is mostly hand-written, but I also make sure I can read it every time I review the file.

## Assumptions

The important part is to not assume skill. Don't say "BitWarden" and a password. Let them know what it is:

> To get to the list of all passwords and credit cards, go into BitWarden on my phone or at https://bitwarden.com/. The user name is XXX and the password is YYY. If you have my phone, you can use Ageis to get the verification key, otherwise this is a list of one-time passwords. If you use this, change the password immediately to something secure; there is a "Generate" built into BitWarden that gives good passwords.

Remember, you don't know who is going to be in the file. You don't know if they are [trusted](./trust.md) or not. It may not be your partner, but it might be your parents or one of your children. You can't assume knowledge, only that someone is trying to prevent your old life from falling apart. So, assume they know nothing.

In the above example, I cross out the password and write the new one and a date below it. And then again and again until I run out of paper. Then I rewrite it and destroy the old one.

The above example is probably a [first day](./time.md) item. Put it right at the beginning. Don't worry about empty space, as the years go by, it will cease to be white.

## Useful Information

-   [Notices](./notices.md)
-   [Password Vaults](./passwords.md)
-   [Bank Accounts](./financial.md)
