# Financial Information

For a lot of us, we have access to financial accounts that our partners, children, and parents don't know about it.

## Credit Cards

As a [first day](./time.md) item, listing all credit card numbers, their PINs, security codes, and zip codes. You should be using a [password vault](./passwords.md), so this list should also include the name of the entry in the vault. If your vault can store the cards and you feel [safe](./security.md) putting it there, you could store it directly and then this part is just a list of entries and directions of how to get it into the vault.

This is a first day item because they may need a credit card to get a hotel in case of an emergency or to rent a vehicle in a hurry.

It would be smart if you always had a credit card with plenty of balance on that, but I mostly go paycheck-to-paycheck so that's a dream. Instead, I write down how to check the balance to let them know which one has a few bucks here and there.

## Bank Accounts

On the other hand, as a first week item, a list of bank accounts is needed. These are used less critically, but knowing where to find a check book and having the account numbers is important. It is also important to be able to log into those accounts so the password vault should have all the information needed to log in.

The [file](./documentation.md), on the other hand, is a great place to put the one-time passwords to get to the bank if they provide them.

Other information related to bank accounts is list where your payroll goes. Do you put a percentage in one bank and the rest in the other? That is important because your final paycheck is probably going to follow the same rules and the survivors might need access to that.

I would also include which accounts have automatic payments coming from them. I try to have all the bills pay themselves from a single, dedicated account. I also include a rough budget so my partner knows how much needs to be there to keep everything running in the first year.

## Insurance

List insurance policies. Every single one, no matter if it is the $1000 from your bank or the company one. Include information on the beneficiaries and how to contact the insurance company. I actually consider this a first week item because red tape can take a while to process and this is relatively "free" money.

Now, we've seen a lot of push on taxes. If you are reading this, you probably don't need to worry about it since the threshold is in the [millions](https://www.irs.gov/businesses/small-businesses-self-employed/estate-tax) according to [many](https://www.fool.com/research/estate-inheritance-taxes/) [websites](https://www.investopedia.com/articles/personal-finance/120715/estate-taxes-who-pays-what-and-how-much.asp).

## Retirement

This is a first month item. Knowing where the 401(k) and IRA is important because that is a buffer for emergencies.

Like accounts, I put these in the password vault and then just list their entries and contact information to get them out. Including the occasional balance also helps.

This information is important to making sure the bills are paid and no one loses the house. It may also be needed just to survive, so it is definitely critical information in the first month.
