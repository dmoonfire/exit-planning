# Funerals

Let your wishes be known for how you want to handle it. This is a personal choice, so much of this part is going to be personal opinions.

Funerals are expensive for what they provide, which is closure for the living. The rituals and organization of them has an emotional toil on your survivors. It is also a very profitable industry.

You need to decide if you want that cost. Because the money spent for an expensive coffin or grand affair is money that can't be spent making sure your loved ones can pay the bills. You need to decide what is important to you and them. If they need a funeral for closure, then talk to them and decide. If you need to know there is a big affair, then that's your choice.

> Dylan's wishes are as simple: cremation as cheap and quick as possible. Donate organs if possible. Have a pot luck dinner or have a night out with friends and family instead. Talk about the good things and remember I love all of you.
>
> I'd rather my partner spend $150 for a cardboard coffin and some legal affairs while being able to use the thousands of dollars that would have been spent on a coffin that would be crushed (thank you MythBusters) the moment it is buried.

As we said, this is a very personal thing. For our point of view, we're done. There is no coming back from the dead at that point and our plans are focused on the survivors for that very reason.
