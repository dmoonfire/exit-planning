# Getting Started

The first steps to coming up with a plan can be overwhelming. Just remember, [start small](./tenants.md) and build up over time. Just a single paper helping is better than nothing. A single letter of affection could make the difference of a broken heart and rekindled resolve.

If you are lost, start with something you can do in ten minutes (and maybe a quick trip to the grocery store):

-   [Set up a password manager](./passwords.md)
-   [Find a large envelope you can seal](./documentation.md)
-   Write down how to get into your passwords including one-time passwords
-   Put it into the envelope and seal it
-   Write "In Case of Death or Incapacitation" on the front
-   Put it somewhere safe
-   [Let someone know](./trust.md)

Every once in a while, you want to expand on the details beyond just the passwords:

-   If you have an idea that someone needs, write it down
-   [Review what you write on occasion and expand on it](./review.md)

While you are doing this, you want to figure out who can you can trust to do things:

-   [Figure out who you can trust to help](./trust.md)
-   [Figure out who can help your trusted folk](./support.md)
-   [Find someone to take care of your children and pets](./children.md)
-   [Select someone to handle financial information](./financial.md)
-   [Write letters and thank them, put it in your file](./compassion.md)
-   Let them know what you are doing
-   [Write up notices and post them various places](./notices.md)

There is also a knot of legal tasks you want to get done. Thankfully, these don't require much effort once they are done, but you'll still want to review them occasion.

-   [Get a will](./legal.md)
-   Figure out your powers of attorneys
-   [Find a way of handling incapacitation](./incapacitation.md)
