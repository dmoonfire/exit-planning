# Caretakers

Being a caretaker is a horrible experience in the United States. They have an incredible amount of stress as they manage medical bills that can go into the hundreds of thousands, insurance paperwork, and just dealing with you (but in a limited form). It is a thankless job that provides little support.

Being a spousal caretaker is even worse because the people who have the money and make the rules seem to think that you are going to be faking your injuries for free money. A lot of the programs to help caretakers don't seem to apply to spouses.

At the same time, if your situation is severe, both you (as someone who is incapacitated) and your caretaker will become someone [persona non grata](https://www.merriam-webster.com/dictionary/persona%20non%20grata). This means that friends and family will drift away in discomfort, people will insist that you get sent to a nursing home all the time, and generally you will cease to exist as an uncomfortable reminder of life's fragility.

This means, to be [compassionate](./compassion.md), you need to be aware of it in your planning and try to spread out the load across multiple people. One person cannot shoulder the burden of taking care of you by themselves. One person should not have to shoulder that burden.

## Bureaucracy

One of the most stressful for most people is dealing with bureaucracy. This includes not only medical bills, but also appointments and schedules. Companies will constantly be trying to change the rules, forget paperwork, or somehow ignore the department next to door. They will send notices that the care is being cut off even as you are negotiating or having assurances that you won't be cut off.

If you can find someone you [trust](./trust.md) that is comfortable with this, ask them to help. Not everyone wants to deal with it, but off-loading this frustrating task from your caretaker will make everyone's life easier.

Ideally, you want to find someone who has attention to detail and doesn't get flustered. If that is the only thing they do, as opposed to taking care of you and everything else, it will make a worlds of difference.

## Caretaker Networks

This is a thing that breaks our heart. We've seen parents driving an hour every day to visit their loved one in a nursing home or friends losing almost everything just to take care of their spouses. In almost every single case, they don't get help unless they pay for it. And when they pay for it, they have trouble keeping that help.

Being incapacitated is horrible for everyone involved. It makes others uncomfortable and they... find excuses not to be there. They don't come by to have time. It gets worse when you have some injuries (like strokes) that drastically change personalities. Sometimes you'll become more aggressive or violent; it isn't your fault but you won't be able to stop them.

> Dylan: A family member drove almost every day to visit their daughter in a nursing home. Alone. The same one did the same for their spouse years later.

So what happens in these situations is that there is a chance you'll be reduced down to a single person who will have to bear the full brunt of what has happened.

There is probably nothing you can do about that.

What you might be able to do is make sure you have people who will at least help. This is a case where a social network is important. Not just one person, but many people helping lift you lessens the difficulties for everyone. Finding those people who are willing to keep taking care of you is, unfortunately, beyond the limits of this documentation because it requires good friends.

> Good friends take a long time to grow.

## Nursing Homes

Now, one option that seems to always be given is nursing homes. They are a choice, but there are two key things: quality of life and burn rate.

Many nursing homes are understaffed which means the employees are running from one problem to another. Being a nurse doesn't pay well because this is an industry that the general populace doesn't really want to think about.

The other is burn rate. Nursing homes cost money, a lot of money. And the moment you can't afford, there is a good chance they'll send you back home. Or toss you to a cheaper (and probably worse) nursing home.

## Medical Trusts

There are programs to help with expenses, but they come with the assumption that everyone is trying to steal. This is where you get the [Medicaid](https://www.medicaid.gov/) requiring you _and your spouse_ to not have money. And, they have a "look back" period of five years which means that you are limited in how much you can prepare ahead.

You can look into a [medical trust](https://www.elderlawanswers.com/medicaid-and-trusts-12004) which is a irrevocable trust that manages the money for you. Because it is irrevocable, it means that you basically fund it and then you never get to see that money again. Naturally, it is affected by the five year look back period which means if you need to make one, you also have to be aware that your loved ones are going to have to pay for your medical costs for five years after moving things to a trust before you can take advantage.

Even though most people don't have the ability, setting aside a large influx of money toward this will help a lot.

It would be good if your [file](./documentation.md) could give directions on how to handle this. Maybe you have a medical trust set up, then that definitely should into your file. Or you want to give the threshold that they should start to set one up knowing it will be five years before you can take advantage of it.

## Negativity

We might be overly negative when it comes to this topic, but that is our experience. It isn't fun, it is horrible. It might be something else in another country (not the United States) or it might not be.

That is why having your legal documents in order is good. If you don't want your loved ones to go through it, make sure you have documented "do not resuscitate" properly and do what you can to reduce the financial burden in case you become incapacitated.
