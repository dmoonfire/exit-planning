# Exit Planning

We plan for the birth of our children but rarely do we prepare ourself for death until it's too late. This plot in a digital garden is a capsule of thoughts, observations, and experiences related to exit planning or preparing for the end of life.

This isn't one long document because the idea is to keep things to reasonable chunks of information. Also, in hopes of being able to find specific topics when needed.

⚠️ We are not lawyers. These is things to be aware of and experiences that have happened. In many cases, you might need a lawyer involved and we're not it. ⚠️

The [introduction](./introduction.md) is a short essay on why planning for the end is important. It's a bit poetic, but seems like an easy way to explain why this was written and its purpose. We also have some [tenants](./tenants.md) which guide the process of creating an exit plan. If you are in a hurry, then you can just look at some [getting started](./getting-started.md) suggestions.

The next section, [circumstances](./circumstances.md), is the beginning to talk about the situations that happens when exit planning was needed. There are also additional issues when discussing [incapacitation](./incapacitation.md). Also, we talk about [time](./time.md) because that is critical to how we organize things.

Following that, you want to consider the [compassionate](./compassion.md) side of things and the [technical details](./documentation.md). These are both important, both to help ease the grief of our passing but also the concrete steps that need to be done next. It also is where thinking about who you [trust to help](./trust.md) and establishing a [support network](./support.md) to help your trusted people.

Some of your priorities are going to be figuring out the [legal](./legal.md) issues, starting with what happens to your [children and pets](./children.md) if you have them. Also, how you want your [funeral](./funeral.md) to be arranged.

Over time, you'll [review](./review.md) this information, set up some basic [security](./security.md) to keep it up to date. This also means making sure your [passwords](./passwords.md) is up to date [financial information](./financial.md).

## Primary Links

The primary URL for this is [https://d.moonfire.us/garden/exit-planning/](https://d.moonfire.us/garden/exit-planning/) but the Git repository is located at [https://gitlab.com/dmoonfire/exit-planning](https://gitlab.com/dmoonfire/exit-planning). If you find typos, want to add things, make corrections, or have discussions. Heading over to the Git repository is a good place.

## License

This book is distributed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license. More info can be found at [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/)

The preferred attribution for this novel is:

> "Exit Planning" by D. Moonfire is licensed under CC BY-NC-SA 4.0

In the above attribution, use the following links:

-   Exit Planning: [https://d.moonfire.us/garden/exit-planning/](https://d.moonfire.us/garden/exit-planning/)
-   D. Moonfire: [https://d.moonfire.us/](https://d.moonfire.us/)
-   CC BY-NC-SA 4.0: [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/)
