# Introduction

Just as everyone is born, everyone also dies. It is a part of the natural cycle of life where things end. Though we plan for a child's birth, with parties and dreams of their future, rarely do we talk about how our lives end. Instead, we leave behind an empty hole in the world, both in the hearts and lives of those around us.

How we go is important to those still behind. Over the years, we frequently prop up things on our lives. We may have acquired them because of our skills or desires. Maybe you do the finances for your family? Or make sure everyone is fed? Or clothed? You might care for your parents or your pets or your children. Maybe you have a business or investments or a church.

What happens to those when you pass? Can the survivors take the reins and keep everything going or will it fall apart, leaving a devastation that could destroy futures?

Planning for how we leave the world is a compassion to those around us. We already know that they will be devastated and heart-broken, but how can we make sure their lives continue when we are no longer around? We document and we plan. We come up with something now, when we aren't in a hospital bed or flipping over in a car. We come up with something now so when it happens, we already know that we're prepared.

This garden plot is written in an informal style but we use a few conventions. "You" refers to the one doing the exit planning, the one trying to prepare. "They" is your survivors. And finally, "we" is everyone including the people behind these words.

> We are all in this together.

The biggest thing to remember: take a deep breath. This isn't a sprint, planning is a cumulation of thoughts and organization then is put down to help others. It's a marathon through some scary places that most of us don't want to remain in. It is important, but not exactly a theme park or a beach.
