# Legal

⚠️ We are not lawyers and this isn't legal advice. ⚠️

There are times when you need something [documented](./documentation.md) but with legal might behind it. The two biggest ones are your will and powers of attorneys.

## Will

Get a will.

No matter how much you plan, the legal system requires something a bit more formal. It also doesn't care much about your [passwords](./passwords.md) or writing [letters to your loved ones](./compassion.md) but it is important for things like children and property.

There are easy ways to do it, but they are dependent on your locale. You might be able to get away with a [simple one at first](./tenants.md) but then expand as needed.

The important parts are:

-   Who has the children
-   Who handles the finances
-   Who has the powers of attorney

They may only have a [day or so](./time.md) to pull it out when it happens.

## Powers of Attorney

We might have strong opinions about [incapacitation](./incapacitation.md), mostly in the social, financial, and emotional costs involved. You may not. But, if it isn't written down, then no one knows. If you have a "do not resuscitate" order, then make sure it is written down and documented.

Knowing your wishes is a first hour type of thing but being able to get the legal documentation seems to be a first day thing.

## Trusts

If you have the means, setting up trusts (including medical trusts) will help things go a long way in helping others.
