# Notices

One of the key parts is making sure that the information is communicated. This can be hard, but you can put notices in various key places to let doctors, police, and emergency folks know where to look. This should be public information and [first hour](./time.md), so you don't want to put passwords. Also give the contact information to your [trusted people](./trust.md).

-   Write it on a card, put it in a zip-lock, and stick it in your freezer. This is inspired by the "In Case of Emergency" (ICE).
-   Write it on a card and put it in the glove compartment of your car.
-   Put it in the emergency banner on your phone.
-   If you have a smaller card, having one in your wallet or purse would help.

Mostly, this is information that a stranger would have a chance of looking and know where to start.
