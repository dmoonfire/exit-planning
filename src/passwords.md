# Passwords

Get a password vault, period. Most people choose bad passwords to start with and don't write them down.

I prefer [BitWarden](https://bitwarden.com/) and pay the $10/year because it lets me share passwords with my Partner, children, and in-laws. It lets me designate Partner as my recovery, which means if I am inaccessible for a month, it unlocks for Partner without needing a one-time password.

There are others, but don't trust browser password saving and make sure you can access your passwords without your phone. This is because it might be destroyed in a car accident, lost in a river, or taken for evidence. You need to make sure there are at least two different ways to get to the password.

Also, use a [good password](https://bitwarden.com/password-strength/) for your vault. I like phrases and sentences instead of random letters. As [XKCD](https://xkcd.com/936/) points out, it is easier to remember "behold I am made of cheese" instead of "a8$jd8298vlk#$" but they have the same basic strength. It's a pain to type though, but you can use biometrics if you feel that is [safe](./security.md).

As a [first day](./time.md) item, the directions in your [file](./documentation.md) should explain how to get into the password vault. That allows you to change your passwords via the vault and not have to write down every password change you make.

## Authenticators

Use authenticators whenver possible instead of SMS. Ideally, the authenticator should have a backup and multiple access. The reason to avoid SMS is because it requires a phone, which may be lost or inaccessible when they need it.

I use BitWarden for the bulk of my authenticators, but I also use [Ageis](https://getaegis.app/) for some critical ones because it is not automatically shared across machines.
