# Reviewing

Trying to write the perfect exit plan is not only overwhelming, it is also impossible. You can't come up with every situation, every possibility, or know the future. The best thing to do is start small and add things over time.

## Periodic Reviews

A good way of doing this is to periodically review the notes. This is just a semi-random event where you just open up your [documentation](./documentation.md) and look for anything out of day, maybe add a page or two, and then seal it back up.

For us, we do this the week we also file taxes.

> "Tis impossible to be sure of anything but Death and Taxes."
>
> --- Christopher Bullock

Storing your documentation in the same places as your taxes makes it easier. It also gives you a reminder without having to "nag" you for months that you should do it.

This is also a good time to change your master password on your [passwords](./passwords.md) or other once-a-year things that need to be done.

## Major Events

For most of us, we can only change our benefits during "life events" such as having a child, losing one, getting married or divorce. These are also good times to review your file. If you have a child, are they in your will and have you written what you want to do?

If you are getting a divorce, there is a good chance you will want to remove your spouse from your [trusted](./trust.md) people list if there a possibility of a conflict.

> Dylan: A family member was in a car accident. Since he didn't have a plan in place or other legal documentation, his care when to his wife... who he was in the middle of a divorce at the time of the accident.

## Skipping

Sometimes, life gets in the way. If you find that you can't do it, then skip it. However, the more skip, the more likely the information in your documentation will become stale and be less than useful.

Remember, death and injury can come at any time. You don't get to choose how you leave, but you can choose what you leave behind.
