# Security

There is one danger of this [documentation](./documentation.md). By its nature, you have to give the "keys to the kingdom" including all the information needed to basically steal your life. But, without them, your survivors can't take over.

This means you need to view how you create, store, and document. This is why putting things in a file box isn't always a good choice, you don't know if someone has been in it. Someone truly dedicated getting into it will possibly do it, but hopefully you be able to detect when someone has gotten in when you [review](./review.md) your documentation.

If someone does get into your file, that's the time you need to go on a password-changing spree and rewriting documentation. Finding out who got in would also be helpful since you need to change it.

## Attack Surface

This is the balancing act with exit planning. You need the information to be available when needed but also reduce the chance of temptation to get into it. The easier you have, the more likely someone can get to it.

One approach is to require a password key or combination lock to get into it. This works if you have a waterproof safe that you keep your documentation. You don't write down the code but give it to your [trusted people](./trust.md). This reduces the number of people who might have access.

> Kenneth has notes for his technical son to recover an encrypted file from his laptop to get his notes. His other son has the password.

Putting these "do not write down" is risky since you are then limiting who can access it. If there is a major accident, like a car collision that puts multiple people in the hospital, everyone who knows the safe combination may be incapacitated.

## Scenarios

This is where you have to come with scenarios and see if you want to plan for them. Come up with what-ifs and try to figure out how you want to handle it. You may come up with "and there is nothing I can do about it" as an answer, but at least you thought about it.

> Dylan: I lived in Cedar Rapids in 2008. I was unable to return home for months with the first one. In 2020, a derecho did significant damage to our house but a co-worker had their entire home destroyed by the winds. If that happens, I'm going to put a second copy of my plan at my in-laws house thirty miles away.

There are a number of ways to handle these scenarios, but coming up with them is dependent on you and your life and situation. But, also remember that if you do things like have a second copy of your plans, you are increasing your attack surface.
