# Support

We touched on this topic in the [incapacitation](./incapacitation.md) post, but death is an uncomfortable topic and there is natural pressures for one person to take on the burden of handling your passing.

> Dylan: My partner had to deal with their father's death by themselves because his brothers simply ignored everything except for showing up at the funeral.

You want to avoid this because it leads to burnout and unhappiness. But, no matter what you do, you can't make someone help but you can at least encourage it by establishing a support network that wants to help you continue.

In your [documentation](./documentation), give suggestions of someone who can help with aspects of your plan. If you have technical parts, list names and phones and emails of your [trusted](./trust.md) technical friends. If you know someone who enjoys breaking through tape, let them know.

Ideally, these friends would volunteer themselves. It doesn't always work out that way and your survivors may not feel comfortable asking. There are no hard and fast rules for that, there aren't even good suggestions because it is based on yours.

## When Friends Don't Work

If something is important to you and you can't count on someone who help out of the kindness of their hearts, you may need to go with someone professional. Lawyers and banks will handle things like finances, you can set up [trusts](./legal.md) to manage accounts, or a whole slew of other things.

These all cost something most of us don't have---money---but it might be important for things you care about.

## Reward Those Who Do

Caring for the end is a thankless task in many ways. If you can find a way to make it give thanks, that will encourage those who do it for their own needs and reward those who do it out of the kindness of their hearts.

This is also how you live your life comes into play. If you are someone who helps, then maybe others will do the same.
