# Tenants

These are rough tenants that drive the philosophy of this document.

## Tenant One: Document

> If it isn't written down, it doesn't exist.

When this plan is needed, they can't just run over and ask you to explain something. There is no hint. If you don't write it down, they have to figure it out, they need someone to help, or it gets lost.

## Tenant Two: Compartmentalize

> Limit what needs to be known in an emergency.

Whoever is going to take over is not you. They don't have your knowledge and they probably haven't been thinking about it as long as you. At the same time, they will be probably dealing with a thousand other things that need to be done. That means we need to partition or compartmentalize our plan so they can confidently be able to push or set aside everything that isn't critical in that moment. That is why we organize based on [time](./time.md) and we don't just make dense list of details to remember.

## Tenant Three: Start Small

Take your time doing this. Start small, write down only a few key things as you think about it, and then let it go for a while. Come [back later](./review.md) and add more information as you go.
