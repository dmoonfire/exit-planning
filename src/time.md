# Time

It won't take long for your [file](./documentation.md) to get overwhelming. Even if it isn't, there are times when you need to know about something and times when it can be pushed aside. This is the [second tenant](./tenants.md):

> Limit what needs to be known in an emergency.

To help with that, it is helpful to let them know what should be important at any given point. Now, there are a lot of ways of doing it, but what works for us is the following:

-   The First Hour
-   The First Day
-   The First Week
-   The First Month
-   The First Year

Obviously, the critical things are handled and life will move on.

## The First Hour

The came up when one of our parents died their sleep. We got the phone call from the officers and one of the questions was: where should we send the body?

Didn't have an answer for that.

So, we asked for a little time and called around to get it. It was scary because we were already devastated and blind-sided by the news and we needed to be functional immediately.

The first hour are questions that need to be asked when there is no time to find the [file](./documentation.md) or look up answers. They need to be answerable in the middle of an emergency room or standing in front of a destroy house. This also means, there needs to only be a limited number of them.

From our experience, these are:

-   The location of our [plan](./documentation.md) including backup locations
-   What are our desires for our [funeral](./funeral.md)
-   Who can take the [children and pets](./children.md)
-   Who can be [trusted](./trust.md) to handle things
-   The location of the [will and powers of attorney](./legal.md)

# The First Day

After the first hour, which is usually handled in shock, the rest of the day is when things get overwhelming. There are probably doctors to talk to, family to inform, and everything else. Like the first hour, they need to be relatively short, critical pieces of information.

However, this is also when someone has time to get your exit plan and start looking through it. Now, it may not be the actual first day, it might take a little while longer, but the key part is to answer questions "how to survive the night."

# The First Week

Unlike the first day, the ones that follow is where things start to go numb. It's where they are finally allowing themselves to realize that you're gone. This is when they are going to feel that aching hole the most and grief is going to be gnawing at their thoughts.

It is also time that they may be sitting in the middle of a floor, trying to puzzle through your file. They are still going to be overwhelmed, so we need to be able to push non-serious things to side (for the first month and first year) and let them focus on "what do I do tomorrow."

# The First Month

Surviving the first week, things start to get a little easier. The immediacy should be lessening which means thoughts start to move into "how do I survive the year?" or "what do I do next?" This is when things like paying bills starts to come into play. There will be phone calls to start shutting down accounts, transferring bills, and basically gathering up the pieces.

One will hope, this is also the point where there is less of a panic. But we still recommend that things that can wait are pushed into the final category, the first year.

A good example is of a first month could be:

-   Where are bills paid and roughly how much
-   Life insurance information to get policies
-   Retirement funds in case loans need to be made
-   Human resources to contact for final checks

# The First Year

The final category is the first year. This is everything else including the annual bills like property taxes or car insurance payments. Hopefully, at this point, they have managed to get on their feet and are able to move on.

This category ends up being a lot of hints and suggestions.
