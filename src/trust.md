# Trust

Trust is a big, thorny thing to think about and one that should be important.

> If someone happens to be, run to my home and erase my browser history.

You have to trust the people who are going to handle your plan. If you pick someone who despises you, they may just toss out everything they don't care about and do their own thing. They may do their own thing anyways, but there is nothing you can really do about it besides find people who probably won't.

## Aspects

Everyone has different aspects of their lives. You may have an alternate person, a secret life, or just have an interest that doesn't appeal to everyone. Not everyone is going to know it and you may not want them to know until the end.

Ideally, you would find someone who is aware of your "entire" life, warts and all. Even if they don't care for it, they can pass it off to a friend in that aspect of your life who can take over. This is a good thing to put in your [documentation](./documentation.md).

> Please ask if Bob can manage my Patreon, SubscribeStar, and OnlyFans accounts. If they do, then talk to them about taking 20% off the top to manage the photo sets in the queue and give you the rest. If they agree, the passwords are in my BitWarden site.

If you can't find someone who will accept the full aspect, you can always create a second (or third) set of documentation for that aspect.

> If I haven't posted anything in six months, please take over. This file contains what you need. Do not let Bob know.

Again, this requires you to have someone who has your best intrest in mind, but it might be a way of keeping that aspect of your life separate.

## Locality

One thing to consider when picking your trusted people is how close they are to you. It is one thing to have someone [take care of your children](./children.md) when they are down the street, it's another thing if it takes them two days to get to you.

On the other hand, if you are picking someone to handle [medical beaucracy](./incapacitation.md). That may be mostly remote but there are also times when you might have to be on the phone or a signature is needed.

## Family

If you don't trust your family, they may take offense at that. They may aggressively disagree.

Also, in the abcense of proper documentation, the legal system will typically revert most of exit planning to your family (spouses, parent, children). This means there might be a fight, but sadly there are little placess to register the desinated contacts for this so you may be leaving a fight behind.

The only thing we can think about is let enough people to know that you have an exit plan and who to contact. If your family has a problem, then you might need more in your social network to be able to call them out on it and direct them to something that might be more [legal](./legal.md).
